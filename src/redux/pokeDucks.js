import Axios from "axios";

//Constantes

const dataInicial = {
  count: 0,
  next: null,
  previous: null,
  results: [],
};

//types

const OBTENER_POKEMONES_EXITO = "OBTENER_POKEMONES_EXITO";
const OBTENER_MAS_POKEMONES_EXITO = "OBTENER_MAS_POKEMONES_EXITO";
const OBTENER_ANT_POKEMONES_EXITO = "OBTENER_ANT_POKEMONES_EXITO";
const OBTENER_POKE_INFO_EXITO = "OBTENER_POKE_INFO_EXITO";

//Reducer

export default function pokeReducer(state = dataInicial, action) {
  switch (action.type) {
    case OBTENER_POKEMONES_EXITO:
      return {
        ...state,
        ...action.payload,
      };
    case OBTENER_MAS_POKEMONES_EXITO:
      return {
        ...state,
        ...action.payload,
      };
    case OBTENER_ANT_POKEMONES_EXITO:
      return {
        ...state,
        ...action.payload,
      };
    case OBTENER_POKE_INFO_EXITO:
      return {
        ...state,
        unPokemon: action.payload,
      };
    default:
      return state;
  }
}

//Accion
export const unPokemonDetalleAccion = (
  url = "https://pokeapi.co/api/v2/pokemon/6"
) => async (dispatch) => {
  if (localStorage.getItem(url)) {
    dispatch({
      type: OBTENER_POKE_INFO_EXITO,
      payload: JSON.parse(localStorage.getItem(url)),
    });
  } else {
    try {
      const res = await Axios.get(url);
      // console.log(res);
      dispatch({
        type: OBTENER_POKE_INFO_EXITO,
        payload: {
          nombre: res.data.name,
          ancho: res.data.weight,
          alto: res.data.height,
          foto: res.data.sprites.front_default,
        },
      });
      localStorage.setItem(
        url,
        JSON.stringify({
          nombre: res.data.name,
          ancho: res.data.weight,
          alto: res.data.height,
          foto: res.data.sprites.front_default,
        })
      );
    } catch (error) {
      console.log(error);
    }
  }
};

export const obtenerPokemonesAccion = () => async (dispatch) => {
  if (localStorage.getItem("offset=0")) {
    dispatch({
      type: OBTENER_POKEMONES_EXITO,
      payload: JSON.parse(localStorage.getItem("offset=0")),
    });
    return;
  } else {
    try {
      const res = await Axios.get(
        "https://pokeapi.co/api/v2/pokemon?limit=10&offset=0"
      );
      dispatch({
        type: OBTENER_POKEMONES_EXITO,
        payload: res.data,
      });
      localStorage.setItem("offset=0", JSON.stringify(res.data));
    } catch (error) {
      console.log(error);
    }
  }
};

export const siguientesPokemonesAccion = () => async (dispatch, getState) => {
  const { next } = getState().pokemones;
  //console.log(getState());

  if (localStorage.getItem(next)) {
    dispatch({
      type: OBTENER_MAS_POKEMONES_EXITO,
      payload: JSON.parse(localStorage.getItem(next)),
    });
  } else {
    try {
      const res = await Axios.get(next);
      dispatch({
        type: OBTENER_MAS_POKEMONES_EXITO,
        payload: res.data,
      });
      localStorage.setItem(next, JSON.stringify(res.data));
    } catch (error) {
      console.log(error);
    }
  }
};

export const anterioresPokemonesAccion = () => async (dispatch, getState) => {
  const { previous } = getState().pokemones;
  if (localStorage.getItem(previous)) {
    dispatch({
      type: OBTENER_ANT_POKEMONES_EXITO,
      payload: JSON.parse(localStorage.getItem(previous)),
    });
  } else {
    try {
      const res = await Axios.get(previous);
      dispatch({
        type: OBTENER_ANT_POKEMONES_EXITO,
        payload: res.data,
      });
      localStorage.setItem(previous, JSON.stringify(res.data));
    } catch (error) {
      console.log(error);
    }
  }
};
