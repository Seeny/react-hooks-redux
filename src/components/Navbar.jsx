import React from "react";
import { Link, NavLink, withRouter } from "react-router-dom";
//Redux
import { useDispatch, useSelector } from "react-redux";
import { cerrarSesionAccion } from "../redux/usuarioDucks";

const Navbar = (props) => {
  const dispatch = useDispatch();
  const activo = useSelector((store) => store.usuario.activo);

  const cerrarSesion = () => {
    dispatch(cerrarSesionAccion());
    props.history.push("/login");
  };

  return (
    <div className="navbar navbar-dark bg-dark">
      <Link className="navbar-brand" to="/">
        APP POKE
      </Link>
      <div className="d-flex">
        {activo ? (
          <>
            <NavLink className="btn btn-dark mr-2" exact to="/">
              Inicio
            </NavLink>

            <button
              type="button"
              className="btn btn-dark mr-2"
              onClick={() => cerrarSesion()}
            >
              Cerrar Sesion
            </button>
          </>
        ) : (
          <NavLink className="btn btn-dark mr-2" to="/login">
            Login
          </NavLink>
        )}
      </div>
    </div>
  );
};

export default withRouter(Navbar);
