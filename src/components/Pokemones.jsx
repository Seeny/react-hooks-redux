import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  obtenerPokemonesAccion,
  siguientesPokemonesAccion,
  anterioresPokemonesAccion,
  unPokemonDetalleAccion,
} from "../redux/pokeDucks";
import Detalle from "./Detalle";

const Pokemones = () => {
  const dispatch = useDispatch();
  const pokemones = useSelector((store) => store.pokemones.results);
  const next = useSelector((store) => store.pokemones.next);
  const previous = useSelector((store) => store.pokemones.previous);

  useEffect(() => {
    const fetchData = () => {
      dispatch(obtenerPokemonesAccion());
    };
    fetchData();
  }, [dispatch]);

  //console.log(pokemones);
  return (
    <div className="row mt-5">
      <div className="col-md-6">
        <h3>Lista de pokemones</h3>

        <ul className="list-group mt-4">
          {pokemones.map((item) => (
            <li className="list-group-item text-capitalize" key={item.name}>
              {item.name}
              <button
                onClick={() => dispatch(unPokemonDetalleAccion(item.url))}
                className="btn btn-dark btn-sm float-right"
              >
                Info
              </button>
            </li>
          ))}
        </ul>
        <div className="d-flex justify-content-between mt-3">
          {pokemones.length === 0 && (
            <button
              className=" btn btn-dark "
              onClick={() => dispatch(obtenerPokemonesAccion())}
            >
              Get Pokemones
            </button>
          )}

          {previous && (
            <button
              className=" btn btn-dark "
              onClick={() => dispatch(anterioresPokemonesAccion())}
            >
              Anterior
            </button>
          )}
          {next && (
            <button
              className=" btn btn-dark "
              onClick={() => dispatch(siguientesPokemonesAccion())}
            >
              Siguiente
            </button>
          )}
        </div>
      </div>
      <div className="col-md-6">
        <h3>Detalle de los pokemon</h3>
        <Detalle />
      </div>
    </div>
  );
};

export default Pokemones;
