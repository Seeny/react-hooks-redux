import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { unPokemonDetalleAccion } from "../redux/pokeDucks";

const Detalle = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchData = () => {
      dispatch(unPokemonDetalleAccion());
    };
    fetchData();
  }, [dispatch]);

  const unPokemon = useSelector((store) => store.pokemones.unPokemon);
  //console.log(unPokemon);

  return unPokemon ? (
    <div className="card mt-4 text-center">
      <div className="card-body">
        <img src={unPokemon.foto} alt="" className="img-fluid" />
        <div className="card-title text-capitalize">{unPokemon.nombre}</div>
        <p className="card-text">
          Alto: {unPokemon.alto} - Ancho:{unPokemon.ancho}
        </p>
      </div>
    </div>
  ) : null;
};

export default Detalle;
