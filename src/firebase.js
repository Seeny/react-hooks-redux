import firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBt0FIOEShkGWgL9aAcVGdcW5MyO5MDizs",
  authDomain: "crud-react-7477c.firebaseapp.com",
  databaseURL: "https://crud-react-7477c.firebaseio.com",
  projectId: "crud-react-7477c",
  storageBucket: "crud-react-7477c.appspot.com",
  messagingSenderId: "121884803161",
  appId: "1:121884803161:web:65982fded821748e28f148",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

export { auth, firebase };
